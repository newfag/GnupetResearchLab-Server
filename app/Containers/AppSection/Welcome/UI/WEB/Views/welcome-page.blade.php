<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="apple-touch-icon" sizes="180x180" href="/icons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/icons/favicon-16x16.png">
    <link rel="manifest" href="/icons/site.webmanifest">
    <title>Gnupet Research Lab :: Сервер</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html,
        body {
            background-color: #222831;
            color: #EEEEEE;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 18px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 50px;
            font-weight: bold;
            text-transform: uppercase;
            color: #ffd369;
        }

        .links>a {
            color: #EEEEEE;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .button {
            border: 1px solid #ffd369;
            width: 60px;
            height: 30px;
            border-radius: 5px;
            color: #EEEEEE;
            background-color: #393e46;
            line-height: 30px;
            font-size: 14px;
            text-decoration: none;
            font-weight: bold;
        }

        hr.rounded {
            border-top: 1px solid #ffd369;
            border-radius: 50px;
        }
    </style>
</head>

<body>
    <div class="flex-center position-ref full-height">
        <div class="content">
            @guest
            <a href="{{ route('login') }}" class="top-right button">Login</a>
            @endguest

            @auth
            <form id="form" action="{{  route('post_logout') }}" method="POST">@csrf</form>
            <a class="top-right button" href="javascript:void(0)" onclick="document.getElementById('form').submit()">Logout</a>
            @endauth

            <div class="title m-b-md">Gnupet Research Lab</div>

            <div class="links m-b-md">
                <a href="http://client.gnupet.local/">Client</a>
            </div>

            <div class="links">
                <a href="{{ route('private_docs') }}">Api Documentation</a>
            </div>
        </div>
    </div>
</body>

</html>