<?php

namespace App\Containers\AppSection\Documentation\Actions;

use App\Containers\AppSection\Documentation\Tasks\GenerateAPIDocsTask;
use App\Containers\AppSection\Documentation\Tasks\GetAllDocsTypesTask;
use App\Containers\AppSection\Documentation\Tasks\RenderTemplatesTask;
use App\Containers\AppSection\Documentation\UI\CLI\Commands\GenerateApiDocsCommand;
use App\Ship\Parents\Actions\Action;

class GenerateDocumentationAction extends Action
{
	public function run(GenerateApiDocsCommand $console): void
	{
		// parse the markdown file.
		app(RenderTemplatesTask::class)->run();

		// get docs types that needs to be generated by the user base on his configs.
		$types = app(GetAllDocsTypesTask::class)->run();

		$console->info("Generating API Documentations for (" . implode(' & ', $types) . ")\n");

		// for each type, generate docs.
		$documentationUrls = array_map(static function ($type) use ($console) {
			return app(GenerateAPIDocsTask::class)->run($type, $console);
		}, $types);

		$console->info("Done! You can access your API Docs at: \n" . implode("\n", $documentationUrls));
	}
}
