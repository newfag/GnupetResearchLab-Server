<?php

namespace App\Containers\AppSection\Documentation\Traits;

use App\Containers\AppSection\Authentication\Tasks\GetAuthenticatedUserTask;

trait HasDocAccessTrait
{
    /**
     * Check if the authenticated user has proper
     * roles/permissions to access the private docs
     */
    public function hasDocAccess(): bool
    {
        if (config('appSection-documentation.protect-private-docs')) {
	        $user = app(GetAuthenticatedUserTask::class)->run();
            if ($user !== null) {
                if ($user->hasAnyRole(['admin'])) {
                    return true;
                }
                if ($user->checkPermissionTo('access-private-docs')) {
                    return true;
                }
            }
            return false;
        }

        return true;
    }
}
