<?php

use App\Containers\AppSection\Documentation\UI\WEB\Controllers\Controller;
use Illuminate\Support\Facades\Route;

if (config('appSection-documentation.protect-private-docs')) {
	Route::get(config('appSection-documentation.types.private.url'), [Controller::class, 'showPrivateDocs'])
		->name('private_docs')
		->middleware('auth:web');
} else {
	Route::get(config('appSection-documentation.types.private.url'), [Controller::class, 'showPrivateDocs'])
		->name('private_docs');
}
